# PVE scripts

This is mainly a collection of scripts to bootstrap a new VM. Actually, it do not depends on Proxmox environment.

## Get Started

On a freshly deployed VM, connect with SSH. Then enter

```bash
wget https://gitlab.com/cadegenn/pve/-/raw/master/scripts/bootstrap.sh | sudo bash
```
