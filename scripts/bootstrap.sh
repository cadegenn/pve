#!/bin/bash

BASHFW_VERSION=1.1.1.14
wget -c https://gitlab.com/cadegenn/bash.fw/-/jobs/artifacts/develop/raw/bashfw-${BASHFW_VERSION}-all.deb?job=build_deb_job -O /tmp/bashfw-latest-all.deb

dpkg -i bashfw-latest-all.deb

git clone git@gitlab.com:cadegenn/pve.git

./pve/scripts/init.sh -d -dev
