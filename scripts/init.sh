#!/bin/bash

## @var BASENAME
## @brief basename of the current script being run
declare BASENAME=

## @var DIRNAME
## @brief absolute pathname of the current script being run
declare DIRNAME=

# compute absolute path of caller
if [ -L "$0" ]; then
	BASENAME=$(basename "${SCRIPTNAME}")
	DIRNAME=$(cd "`dirname "${SCRIPTNAME}"`"; pwd)
else
	BASENAME=$(basename "$0")
	DIRNAME=$(cd `dirname "$0"`; pwd)
fi

## @var BASHFW_PATH
## @brief path to installed bash framework
declare BASHFW_PATH=

# try to find BASHFW_PATH in reversed-precedence order
# 99. ${DIRNAME} (default value)
BASHFW_PATH="${DIRNAME}"
# 1. /etc/profile.d/bashfw.sh
[ -e /etc/profile.d/bashfw.sh ] && . /etc/profile.d/bashfw.sh

## @var QUIET
## @brief instruct script to print nothing
## @note	QUIET supersedes all other log level informations like VERBOSE, DEBUG and DEVEL
##			However, it can still log to a file or to a syslog if defined
## @note	QUIET imply YES. So be careful specifying default values.
declare QUIET=

## @var VERBOSE
## @brief instruct script to print verbose informations
declare VERBOSE=

## @var DEBUG
## @brief instruct script to print debug informations
declare DEBUG=

## @var DEVEL
## @brief instruct script to print development informations
declare DEVEL=

## @var YES
## @brief if not empty assume 'yes' to all questions
declare YES=

## @var ASK
## @brief if not empty ask for confirmation before anything is executed
declare ASK=

## @var BASHFW_THEME
## @brief specify color theme for pretty output
declare BASHFW_THEME=ansi

## @var ARGV
## @brief array of user parameters
declare -a ARGV

## @var TRACE
## @brief instruct script to print stack trace on error
## @note  stack trace is always displayed on fatal error
declare TRACE=

## @var FORCE
## @brief	use force to bypass things or force things
declare FORCE=

## @fn usage()
## @brief Print usage informations of common parameters
usage() {
	cat <<- EOF
		COMMON PARAMETERS: ${BASENAME} [-u -q -v -d -dev -ask -y -s -nc -theme theme]
		 	-u		common usage screen (this screen)
		 	-q		quiet: do not print anything to the console
		 	-v		verbose mode: print more messages
		 	-d		debug mode: print VARIABLE=value pairs
		 	-dev	devel mode: print additional development data
		 	-ask	ask for user confirmation for each individual eexec() call
		 			NOTE: it is not affected by -y
		 	-trace	print stack trace on error
		 	-y		assume 'yes' to all questions
		 	-s		simulate: do not really execute commands
		 	-nc		no color. Do not use color theme.
		 			-nc is a shortcut for -theme nocolor
		 	-theme 	use specified theme as color scheme.
		 			use '-theme list' to list available themes.
		 	-force	force things

	EOF
}

# Clean som stuff on signal
onSignal() {
    local rc=$?
	# eerror "Stop on signal received (${rc})."
	# [ "${WORKINGDIR}" ] && [ -e "${WORKINGDIR}/mount" ] && Umount-Chroot "${WORKINGDIR}/mount"
    exit ${rc}
}

# Clean som stuff on exit
onExit() {
    local rc=$?
    [ "${TMP}" ] && [ -e ${TMP} ] && rm -Rf ${TMP}
    exit ${rc}
}

# Set traps
trap onExit EXIT
trap onSignal HUP INT KILL TERM

# parse command line
while [ $# -gt 0 ]; do
	case $1 in
		-q)		QUIET=true
				ASK=
				YES=true
				VERBOSE=
				DEBUG=
				DEVEL=
				TRACE=
				;;
		-v)		VERBOSE=true
				;;
		-d)		DEBUG=true
				VERBOSE=true
				;;
		-dev)	DEVEL=true
				DEBUG=true
				VERBOSE=true
				;;
		-u)		usage
				;;
		-y)		YES=true
				;;
		-ask)	ASK=true
				;;
		-trace)	TRACE=true
				;;
		-s)		SIMULATE=true
				;;
		-nc)	BASHFW_THEME="nocolor"
				;;
		-theme)	shift
				case "${1}" in
					list)
							find ${BASHFW_PATH}/lib/ -name "theme_*.rc" -printf "%f\n" | cut -d '_' -f2 | cut -d '.' -f1 | tr -s '\n' ' '
							echo # if this echo is not present, the above line do not display anything
							;;
					*)		BASHFW_THEME="${1}"
							;;
				esac
				;;
		-force)	FORCE=true
				;;
		*)		# store user-defined command line-arguments
				ARGV=("${ARGV[@]}" "${1}")
				;;
	esac
	shift
done

# load API
source "${BASHFW_PATH}/lib/api.rc" || {
	echo "${BASHFW_PATH}/lib/api.rc not found... Aborting."
	exit 1
}

## @fn main()
## @brief	function containing main program
main() {

	eenter "${FUNCNAME}()"

	 ######  ########    ###    ########  ######## 
	##    ##    ##      ## ##   ##     ##    ##    
	##          ##     ##   ##  ##     ##    ##    
	 ######     ##    ##     ## ########     ##    
	      ##    ##    ######### ##   ##      ##    
	##    ##    ##    ##     ## ##    ##     ##    
	 ######     ##    ##     ## ##     ##    ##    

	## @var		ROOTDIR
	## @brief	root directory of project
	export ROOTDIR=$(cd ${DIRNAME}/..; pwd)

	## @var		SKIPPASSLIST
	## @brief	List of passes to execute, by number
	## @note	If empty, all passes are executed
	declare SKIPPASSLIST=

	## @var		PASSLIST
	## @brief	List of passes to execute, by number
	## @note	If empty, all passes are executed
	declare PASSLIST=

	help() {
		cat <<- EOF
			DESCRIPTION: ${BASENAME} build a Debian-based LinuxPE distrib
			USAGE: ${BASENAME} [-h -H]
			 	-h		show help (this screen)
			 	-H		show help (this screen) with common parameters usage
			 	--skip-pass
			 			comma separated list of passes to skip. use the numbers.
			 			to known all available passes, use --list-pass parameter
			 	--exec-pass
			 			comma separated list of passes to execute. use the numbers.
			 			to known all available passes, use --list-pass parameter
			 	--list-pass
			 			list all passes available

		EOF
	}

	# parsing user-defined command line arguments
	while [ $# -gt 0 ]; do
	    case $1 in
			-h)
				help
				exit
				;;
			-H)
				help
				usage
				exit
				;;
			--skip-pass)
				shift
				IFS=',' read -r -a SKIPPASSLIST <<< "${1}"
				;;
			--exec-pass)
				shift
				IFS=',' read -r -a PASSLIST <<< "${1}"
				;;
			--list-pass)
				for f in $(find ${ROOTDIR}/passes/ -mindepth 1 | sort); do
					filename=$(basename $f)
					PNUM=${filename%%_*}
					PNAME=${filename##*_}
					PNAME=${PNAME%%.*}
					einfo "${PNUM} - ${PNAME}"
				done
				echo
				exit 0
				;;
			*)	efatal "unknown argument ${1}"
				;;
		esac
		shift
	done

	source "/etc/os-release" || efatal "/etc/os-release not found."

	# [ ! "${CONFIG_FILE}" ] && efatal "You have to provide a config file. See help."

	if [ "${PASSLIST}" ]; then
		ewarn "**"
		ewarn "List of pass to execute overriden on command line !"
		ewarn "**"
	fi
	for f in $(find "${ROOTDIR}/passes" -name "*.rc" | sort); do
		ebreak
		filename=$(basename $f)
		PNUM=${filename%%_*}
		PNAME=${filename##*_}
		PNAME=${PNAME%%.*}
		etitle "#${PNUM}. ${PNAME}"
		if [ "${SKIPPASSLIST}" ]; then
			PASSFOUND=
			for i in "${SKIPPASSLIST[@]}"; do
				if [ "${i#0}" -eq "${PNUM#0}" ] ; then
					PASSFOUND=true
				fi
			done
			if [ "${PASSFOUND}" ]; then
				ewarn "Pass #${PNUM} - ${PNAME} \t - skipped as requested"
				continue
			fi
		fi
		if [ "${PASSLIST}" ]; then
			PASSFOUND=
			for i in "${PASSLIST[@]}"; do
				if [ "${i#0}" -eq "${PNUM#0}" ] ; then
					PASSFOUND=true
				fi
			done
			if [ ! "${PASSFOUND}" ]; then
				ewarn "Pass #${PNUM} - ${PNAME} \t - skipped. This pass is not requested"
				continue
			fi
		fi

		# execute pre-hook
		PREHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${VERSION_CODENAME,,}/${PNUM}_${PNAME}.aa.rc"
		[ ! -e "${PREHOOK}" ] && PREHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${PNUM}_${PNAME}.aa.rc"
		if [ -e "${PREHOOK}" ]; then
			ebegin "Execute pre-hook $(basename ${PREHOOK})"
			edevel "-> ${PREHOOK}"
			source "${PREHOOK}"
		fi

		PASSHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${VERSION_CODENAME,,}/${PNUM}_${PNAME}.rc"
		[ ! -e "${PASSHOOK}" ] && PASSHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${PNUM}_${PNAME}.rc"
		if [ -e "${PASSHOOK}" ]; then
			ebegin "Execute pass override $(basename ${PASSHOOK})"
			edevel "-> ${PASSHOOK}"
			source "${PASSHOOK}"
		else
			ebegin "Execute pass $(basename ${f})"
			edevel "-> ${f}"
			source "$f"
		fi

		# execute post-hook
		POSTHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${VERSION_CODENAME,,}/${PNUM}_${PNAME}.zz.rc"
		[ ! -e "${POSTHOOK}" ] && POSTHOOK="${ROOTDIR}/hooks/${HOSTNAME,,}/${PNUM}_${PNAME}.zz.rc"
		if [ -e "${POSTHOOK}" ]; then
			ebegin "Execute post-hook $(basename ${POSTHOOK})"
			edevel "-> ${POSTHOOK}"
			source "${POSTHOOK}"
		fi

		# eend $?

	done

	ebreak
	esumwarnings
	ebreak
	esumerrors
	ebreak

	######## ##    ## ########  
	##       ###   ## ##     ## 
	##       ####  ## ##     ## 
	######   ## ## ## ##     ## 
	##       ##  #### ##     ## 
	##       ##   ### ##     ## 
	######## ##    ## ########  

	eleave "${FUNCNAME}()"

}

main "${ARGV[@]}"
unset ARGV
echo
